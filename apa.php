<?php
require_once __DIR__."/vendor/autoload.php";

use ApaiIO\Configuration\GenericConfiguration;
use ApaiIO\ApaiIO;
use ApaiIO\Operations\Lookup;
use ApaiIO\Operations\BrowseNodeLookup;

class ApaOperation{
	public function __construct($country, $key, $secret, $aTag){
		$this->conf = new GenericConfiguration();
		$this->conf ->setCountry($country)
    		  		->setAccessKey($key)
    		  		->setSecretKey($secret)
    		  		->setAssociateTag($aTag);
    	$this->apaiIo = new ApaiIO($this->conf);
	}

	public function makeItemLookUpCall($itemIds,$response_group = array('Images','SalesRank','ItemAttributes','Offers','VariationSummary','Reviews')){
		$result = null;
		while(true){
			try
			{
				$response = $this->itemLookUp($itemIds, $response_group);
				$results = $this->parseItemLookUpResponse($response);
			}
			catch (Exception $e)
			{
				$message = json_decode($e->getMessage());
				var_dump($e->getMessage());
				if (!empty($message->Error)){
					$invalidAsins = $this->_getInvalidAsinsFromError($message->Error);
					$newItemIds = array_diff($itemIds, $invalidAsins);
					$response = $this->itemLookUp($newItemIds, $response_group);
					$results = $this->parseItemLookUpResponse($response);
				}
				else{
					sleep(5);
					continue;
				}
			}
			if (!empty($results)){
				return $results;
			}
		}
	}

	public function itemLookUp($itemIds, $response_group = array('Images','SalesRank','ItemAttributes','Offers','VariationSummary','Reviews')){
		if (is_array($itemIds))$itemIds = join(',' ,$itemIds);
		$lookup = new Lookup();
		$lookup->setItemId($itemIds);
		$lookup->setResponseGroup($response_group); 
		$itemsLookUpResponse = $this->apaiIo->runOperation($lookup); 
		return $itemsLookUpResponse;
	}

	public function browseNodeLookup($nodeId, $response_group){
		$browseNodeLookup = new BrowseNodeLookup();
		$browseNodeLookup->setNodeId($nodeId);
		$browseNodeLookup->setResponseGroup($response_group); 
		$response = $this->apaiIo->runOperation($browseNodeLookup);
		return $response;
	}

	public function parseItemLookUpResponse($itemsLookUpResponse){
		$items_info = simplexml_load_string($itemsLookUpResponse);
		try{
			$this->_validateResponse($items_info);
		}catch(Exception $e){
			throw $e;
		}
		$items = $items_info->Items->Item;
		$result = array();
		foreach($items as $item) {
			$result[(string)$item->ASIN] = array(
										 "asin" =>(string)$item->ASIN,
										 "rank"=> $item->SalesRank ? (int)$item->SalesRank:999999,
										 "url" =>(string)$item->DetailPageURL,
										 "image_url"=> (string)$item->LargeImage->URL,
										 "title" => (string)$item->ItemAttributes->Title,
										 "category" => (string)$item->ItemAttributes->ProductGroup,
										 "created_at" => date("Y-m-d H:i:s"),
										 "list_price" => $this->_getListPrice($item),
										 "price" => $this->_getPrice($item),
										 "lowest_price" => $this->_getLowestPrice($item),
										 "highest_price" => $this->_getHighestPrice($item),
										 "review_link" => $this->_getReviewLink($item)
												);
		}
		return $result;
	}

	private function _getListPrice($item){
		if (!empty($item->ItemAttributes->ListPrice)){
			return ($item->ItemAttributes->ListPrice->Amount) * 0.01;
		}else{
			return 0;
		}
	}

	private function _getLowestPrice($item){
		if (!empty($item->VariationSummary)){
			return ($item->VariationSummary->LowestPrice->Amount) * 0.01;
		}
		if ($item->OfferSummary->LowestNewPrice){
			return ($item->OfferSummary->LowestNewPrice->Amount) * 0.01;
		}
		else{
			return 0;
		}
	}

	private function _getHighestPrice($item){
		if (!empty($item->VariationSummary)){
			return $item->VariationSummary->HighestPrice->Amount * 0.01;
		}
		if (!empty($item->Offers->Offer)){
			return $item->Offers->Offer->OfferListing->Price->Amount * 0.01;
		}
		else{
			return 0;
		}
	}

	private function _getPrice($item){
		if (!empty($item->Offers->Offer)){
			return $item->Offers->Offer->OfferListing->Price->Amount * 0.01;
		}
		else{
			return 0;
		}
	}

	private function _getReviewLink($item){
		if (!empty($item->CustomerReviews) && $item->CustomerReviews->HasReviews == 'true'){
			return $item->CustomerReviews->IFrameURL;
		}
		else{
			return "";
		}
	}

	private function _validateResponse($items_info){
		if (empty($items_info)){
			throw new Exception("Please pass xml response to this function");
		}
		
		if (strtolower($items_info->Items->Request->IsValid) != "true"){
			var_dump($items_info->Items->Request);
			throw new Exception("request is not valid");
		}
		if (!empty($items_info->Items->Request->Errors)){
			// var_dump($items_info->Items->Request->Errors->Error->Code);
			throw new Exception(json_encode($items_info->Items->Request->Errors));
		}
	}

	private function _getInvalidAsinsFromError($errors){
		$dirtyAsins = array();
		if (!is_array($errors)){
			$errors = [$errors];
		}
		foreach($errors as $error){
			if($error->Code == "AWS.InvalidParameterValue"){
				preg_match(
					"/^([0-9A-Za-z]*)\s+is\s+not\s+a\s+valid\s+value\s+for\s+ItemId/",
					$error->Message,
					$match
					);
				if (!empty($match)){
					$dirtyAsins[] = $match[1];
				}
			}
		}
		return $dirtyAsins;
	}
}





