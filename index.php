<?php
ini_set('memory_limit','128M');
ini_set('max_execution_time', 0);

require_once __DIR__."/apa.php";
require_once __DIR__."/db.php";
require_once __DIR__."/ReviewPage.php";

$asinFile = __DIR__."/../public/asin.txt";
$notInFile = __DIR__."/../public/not_in_asin.txt";
$progressFile = __DIR__."/progress.json";
$config = file_get_contents(__DIR__.'/config.json');
$config = json_decode($config);

date_default_timezone_set($config->timezone);
$servername = $config->db->servername;
$username = $config->db->username;
$password = $config->db->password;
$dbname = $config->db->dbname;
$port = $config->db->port;

$operation = new ApaOperation(
								$config->apa->country,
							    $config->apa->key,
							    $config->apa->secret,
							    $config->apa->aTag
							  );

$db = new DB(
			$servername, 
			$username, 
			$password, 
			$dbname, 
			$port
			);

/* --------test -----------
$db->truncateTable('slip_products');
$db->dumpSlipAsinsToSlipTable();
$results = $db->getSlipProducts();
if (!empty($results)){
	$count = 0;
	$flag = 0;
	$itemIds = array();
	foreach ($results as $key => $value) {
		$itemIds[] = $value;
		$flag++;
		$count++;
		if ($flag == 10 || $count == count($results)){
			$itemLookUpresults = $operation->makeItemLookUpCall($itemIds);
			$db->insertRecordsToSlipTable($itemLookUpresults);
			$flag = 0;
			$itemIds = [];
		}
	}
}
 --------test -----------*/

$lines = file($asinFile);
$tenFlag = 0;
$tenArray = array();
$progress = json_decode(file_get_contents($progressFile));
if ($progress->line == -1){
	$db->truncateTable('tmp');
}

foreach($lines as $line_num=>$line){
	$progress = json_decode(file_get_contents($progressFile));
	if ($line_num <= $progress->line)continue;
	$start = microtime(true);
	$line_array = explode("\t" , $line);
	$tenArray[$line_array[0]] = array(
									 'asin' => (string)trim($line_array[0]), 
									 'review_star'=>(float)$line_array[1], 
									 'review_num'=>(int)$line_array[2]
									 );
	$tenFlag++;
	if ($tenFlag == 10 || $line_num == count($lines)){

		$itemIds = array_keys($tenArray);
		//$response = $operation->itemLookUp($itemIds);
		$itemLookUpresults = $operation->makeItemLookUpCall($itemIds);
		printf("call amazon ads api costs: %f s\n", microtime(true) - $start);
		// try{
		// 	$itemLookUpresults = $operation->parseItemLookUpResponse($response);
		// }catch (Exception $e){
		// 	printf($e->getMessage());
		// 	die("die");
		// }
		$results = array();
		foreach($tenArray as $asin=>$value){
			if (!empty($itemLookUpresults[$asin])){
				$results[$asin] = array_merge($value, $itemLookUpresults[$asin]);
			}
		}
		// var_dump($results);
		$db_time = microtime(true);
		$db->insertRecordsToTmpTable($results, 'tmp');
		printf("insert data costs: %f s\n", microtime(true) - $db_time);
		$tenFlag = 0;
		$tenArray = [];
		$progress->line = $line_num;
		$progress->asin = $line_array[0];
		file_put_contents($progressFile ,json_encode($progress));
		$time_cost = microtime(true)-$start;
		printf("get 10 products info costs: %f s\n", $time_cost);
		if ($time_cost < 1){
			usleep(round((1-$time_cost)*1000));
		}
	}
	
}

$db->dumpFromTmpToProduct();
$db->dumpFromTmpToProductRecord();

$progress->line = -1 ;
$progress->asin = "FFFFFFFFF";
file_put_contents($progressFile ,json_encode($progress));

$db->truncateTable('slip_products');
$db->dumpSlipAsinsToSlipTable();
$results = $db->getSlipProducts();
if (!empty($results)){
	$count = 0;
	$flag = 0;
	$itemIds = array();
	foreach ($results as $key => $value) {
		$itemIds[] = $value;
		$flag++;
		$count++;
		if ($flag == 10 || $count == count($results)){
			$itemLookUpresults = $operation->makeItemLookUpCall($itemIds);
			$db->insertRecordsToSlipTable($itemLookUpresults);
			$flag = 0;
			$itemIds = [];
		}
	}
}


$slipProductReiviews = $db->getSlipProductReview();
foreach ($slipProductReiviews as $asin => $review_link) {
	echo $asin;
	$reviewPage = new ReviewPage($review_link);
	$retry_time = 0;
	while($retry_time <= 5){
		try{
			$reviewPage->request();
			$reviewPage->getReviewNum();
			$db->updateReviewInSlipTable($asin, $reviewPage->getReviewStar(), $reviewPage->getReviewNum());
			break;
		}
		catch (Exception $e){
			var_dump($e->getMessage());
			$retry_time++;
			sleep(5);
		}
	}
}
$db->dumpFromTmpToProduct("slip_products");
$db->dumpFromTmpToProductRecord("slip_products");
// $db->dumpSlipAsinsToSlipTable();
// $results = $db->getSlipProducts();
// var_dump($results);



?>