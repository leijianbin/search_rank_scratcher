<?php
require_once "vendor/autoload.php";
use Sunra\PhpSimple\HtmlDomParser;

class ReviewPage{

	private $link;
	private $review_star;
	private $review_num;

	public function __construct($link){
		$this->link = $link;
	}

	public function setLink($link){
		$this->link = $link;
	}

	public function request(){
		$this->html = HtmlDomParser::file_get_html($this->link);
		if (empty($this->html)){
			throw new Exception("bad request: ".$this->link);
		}
	}

	public function getReviewStar(){
		$reviewImg = $this->html->find(".asinReviewsSummary img", 0);
		if (empty($reviewImg))throw new Exception("can not find review star");
		$reviewText = $reviewImg->title;
		preg_match('/\d+.\d+|\d+/',$reviewText , $matches);
		$this->review_star = $matches[0];
		return $this->review_star;
	}

	public function getReviewNum(){
		$reviewCount = $this->html->find(".crAvgStars a", 1);
		if (empty($reviewCount))throw new Exception("can not find review counts");
		$this->review_num = preg_replace('/[^0-9]/','', $reviewCount->innertext);
		return $this->review_num;
	}

}
?>