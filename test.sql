INSERT IGNORE INTO slip_products(asin) 
(SELECT products.asin 
FROM products
LEFT JOIN tmp
ON products.asin = tmp.asin
WHERE tmp.asin IS NULL);

SELECT asin 
FROM products
WHERE asin NOT IN (SELECT asin FROM tmp);

SELECT asin 
FROM products
WHERE  NOT EXISTS (SELECT asin FROM tmp);

SELECT * 
FROM tmp
WHERE rank BETWEEN 0 AND 1100
AND category = 'Sports'
ORDER BY rank ASC;

dumpFromTmpToProduct

INSERT INTO products(asin,title,url,image_url,rank,created_at)
SELECT asin,title,url,image_url,rank,created_at FROM tmp
WHERE rank BETWEEN 1 AND 1100 AND category='Sports' 
ORDER BY rank ASC ON DUPLICATE KEY  UPDATE 
title=VALUES(title), image_url=VALUES(image_url),rank=VALUES(rank), updated_at = VALUES(created_at);

dumpFromTmpToProductRecord
INSERT INTO product_records(asin,rank,review_num,review_star,list_price,price,lowest_price,highest_price,create_at)
SELECT asin,rank,review_num,review_star,list_price,price,lowest_price,highest_price,created_at FROM tmp
WHERE rank BETWEEN 1 AND 1100 AND category='Sports'


UPDATE products 
LEFT JOIN tmp 
ON products.asin = tmp.asin
SET 