<?php

// Create connection

class DB {
	public function __construct($servername, $username, $password, $dbname, $port){
		$this->conn = new mysqli($servername, $username, $password, $dbname, $port);
		if ($this->conn->connect_error){
			die("Connection failed: " . $this->conn->connect_error);
		}
	}

	public function insertRecordsToTmpTable($records, $table = 'tmp'){
		$fields = array('asin', 'title', 'category', 'url', 
						'image_url', 'rank', 'review_star', 'review_num', 
						'created_at','list_price','price','lowest_price','highest_price');
		$template = sprintf("INSERT IGNORE INTO %s(%s) VALUES ", $table , join($fields, ','));
		$query = $this->_generateBatchInsertQuery($fields,$template,$records);
		if(!$this->conn->query($query)){
			throw new Exception(printf("%s Error message: %s\n", __FUNCTION__,$this->conn->error));
		}
	}

	
	public function insertRecordsToSlipTable($records, $table = "slip_products"){
		$fields = array('asin', 'title', 'category', 'url', 
						'image_url', 'rank','created_at',
						'list_price','price','lowest_price','highest_price','review_link');
		$template = sprintf("REPLACE INTO %s(%s) VALUES ", $table , join($fields, ','));
		$query = $this->_generateBatchInsertQuery($fields,$template,$records);
		if(!$this->conn->query($query)){
			throw new Exception(printf("%s Error message: %s\n", __FUNCTION__,$this->conn->error));
		}
	}

	public function updateReviewInSlipTable($asin, $review_star, $review_num){
		$query = sprintf("UPDATE slip_products SET review_star = %f, review_num = %d WHERE asin = '%s'",(float)$review_star,(int)$review_num, $asin);
		var_dump($query);
		if(!$this->conn->query($query)){
			throw new Exception(printf("%s Error message: %s\n", __FUNCTION__,$this->conn->error));
		}
	}

	public function dumpSlipAsinsToSlipTable(){
		$sql = "INSERT IGNORE INTO slip_products(asin) ";
		$sql.= "(SELECT products.asin FROM products ";
		$sql.= "LEFT JOIN tmp ";
		$sql.= "ON products.asin = tmp.asin AND tmp.rank BETWEEN 0 AND 1100 ";
		$sql.= "WHERE tmp.asin IS NULL);";
		var_dump($sql);
		if(!$this->conn->query($sql)){
			throw new Exception(printf("%s Error message: %s\n", __FUNCTION__,$this->conn->error));
		}
	}

	public function getSlipProducts(){
		$sql = "SELECT asin FROM slip_products";
		$exec = $this->conn->query($sql);
		$result = null;
		if(!$exec){
			throw new Exception(printf("%s Error message: %s\n", __FUNCTION__,$this->conn->error));
		}else{
			while($row = $exec->fetch_assoc())
			$result[]= $row['asin'];
		}
		return $result;
	}

	public function getSlipProductReview(){
		$sql = "SELECT asin,review_link FROM slip_products WHERE review_link !='';";
		$exec = $this->conn->query($sql);
		$result = null;
		if(!$exec){
			throw new Exception(printf("%s Error message: %s\n", __FUNCTION__,$this->conn->error));
		}else{
			while($row = $exec->fetch_assoc())
			$result[$row['asin']]= $row['review_link'];
		}
		return $result;
	}

	public function dumpFromTmpToProduct($table = 'tmp'){
		$sql =  " INSERT INTO products(asin,title,url,image_url,rank,created_at) ";
		$sql .= " SELECT asin,title,url,image_url,rank, created_at FROM ".$table;
		if ($table == 'tmp'){
			$sql .= " WHERE rank BETWEEN 1 AND 1100 AND category='Sports' ";
		}
		$sql .= " ORDER BY rank ASC ON DUPLICATE KEY  UPDATE ";
		$sql .= " title=VALUES(title), image_url=VALUES(image_url),rank=VALUES(rank),updated_at = VALUES(created_at);";
		$this->conn->query($sql);
		if(!$this->conn->query($sql)){
			throw new Exception(printf("%s Error message: %s\n", __FUNCTION__,$this->conn->error));
		}
	}

	public function dumpFromTmpToProductRecord($table='tmp'){
		$sql = "INSERT INTO product_records(asin,rank,review_num,review_star,list_price,price,lowest_price,highest_price,create_at) ";
		$sql .= " SELECT asin,rank,review_num,review_star,list_price,price,lowest_price,highest_price,created_at FROM  ".$table;
		if ($table == 'tmp'){
			$sql .= " WHERE rank BETWEEN 1 AND 1100 AND category='Sports'";
		}
		if(!$this->conn->query($sql)){
			throw new Exception(printf("%s Error message: %s\n", __FUNCTION__,$this->conn->error));
		}
	}

	public function truncateTable($table){
		$sql = "TRUNCATE TABLE ".$table.";";
		if(!$this->conn->query($sql)){
			throw new Exception(printf("%s Error message: %s\n", __FUNCTION__,$this->conn->error));
		}
	}

	public function getDiffAsinFromTwoTable($table1, $table2){
		$sql_format = "SELECT t1.asin 
						FROM %s AS t1
						LEFT JOIN %s AS t2
						ON t1.asin = t2.asin
						WHERE t2.asin IS NULL;";
		$sql = sprintf($sql_format, $table1, $table2);
		$result = null;
		$exec = $this->conn->query($sql);
		if(!$exec){
			throw new Exception(printf("%s Error message: %s\n", __FUNCTION__,$this->conn->error));
		}else{
			while($row = $exec->fetch_assoc())
			$result[]= $row['asin'];
		}
		return $result;
	}

	public function getSearchItems(){
		$sql = "SELECT product_asin, search_term FROM search_rank WHERE active=1 ORDER BY search_term ASC;";
		$result = null;
		$exec = $this->conn->query($sql);
		if(!$exec){
			throw new Exception(printf("%s Error message: %s\n", __FUNCTION__,$this->conn->error));
		}else{
			$token = null;
			while($row = $exec->fetch_assoc()){
				if (empty($token) || $row['search_term'] != $token){
					$token = $row['search_term'];
				}
				$result[strval($token)][] = $row['product_asin'];
			}
			
		}
		return $result;
	}

	public function saveSearchRankResult($asin, $search_term, $rank){
		$format = "INSERT INTO search_rank_record(product_asin, search_term, search_rank, created_at) VALUES ('%s', '%s', '%s', CURRENT_TIMESTAMP)";
		$sql = sprintf($format, $asin, $search_term, $rank);
		$exec = $this->conn->query($sql);
		if(!$exec){
			throw new Exception(printf("%s Error message: %s\n", __FUNCTION__,$this->conn->error));
		}
	}

	private function _generateBatchInsertQuery($fields, $template, $records){
		$batch = array();
		foreach ($records as $key => $value) {
			$batch_string = "(";
			for($i = 0; $i < count($fields); $i++){
				$insertValue = $this->conn->real_escape_string($value[$fields[$i]]);
				if (is_numeric($value[$fields[$i]])){
					$batch_string .= "{$insertValue}";
				}
				else {
					$batch_string .= "'{$insertValue}'";
				}
				if ($i !=  count($fields)-1 ){
					$batch_string .= ",";
				}
			}
			$batch[] = $batch_string.")";
		}
		$query = $template.join($batch, ',');
		return $query;
	}


}


?>