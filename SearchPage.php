<?php
require_once "vendor/autoload.php";

use Sunra\PhpSimple\HtmlDomParser;

class SearchPage{

  private $keyWord;
  private $urlFormat = "http://www.amazon.com/s/ref=sr_pg_%s?page=%s&keywords=%s";
  private $url;

  public function request($keyword, $page){
    $url = $this->url =  sprintf($this->urlFormat, $page, $page, urlencode($keyword));
    $this->html = HtmlDomParser::file_get_html($url);
    if (empty($this->html)){
      throw new HttpRequestException("bad request: ".$url);
    }else{
      return $this->parse();
    }
  }

  public function getUrl(){
    return $this->url;
  }

  public function parse(){
    $items = $this->html->find("#resultsCol a.s-access-detail-page");
    $result = null;
    if (empty($items)){
      throw new Exception("Error Parsing Page ".$this->url);
    }
    foreach($items as $item){
      preg_match("/dp\/(\w*)\/ref/", $item->href, $asins);
      preg_match("/\/ref=sr_\d*_(\d*)\//", $item->href, $ranks);
      if(!empty($asins)){
        $result[$asins[1]] = $ranks[1];
      }
    }
    return $result;
  }

  public function savePage($filename){
    file_put_contents($filename, file_get_contents($this->url));
  }

  private function _parseLink($link){
    preg_match("/dp\/(\w*)\/ref/", $link, $asins);
    preg_match("/\/ref=sr_\d_(\d*)\//", $link, $ranks);
    if(!empty($asins)){
      echo $asins[1];
      echo "\t";
      echo $ranks[1];
    }
  }


}