<?php
ini_set('memory_limit','256M');
ini_set('max_execution_time', 0);
define('MAX_RETRY', 3);

require_once __DIR__."/apa.php";
require_once __DIR__."/db.php";
require_once __DIR__."/SearchPage.php";
$config = file_get_contents(__DIR__.'/config.json');
$config = json_decode($config);

date_default_timezone_set($config->timezone);
$servername = $config->db->servername;
$username = $config->db->username;
$password = $config->db->password;
$dbname = $config->db->dbname;
$port = $config->db->port;

$operation = new ApaOperation(
                  $config->apa->country,
                  $config->apa->key,
                  $config->apa->secret,
                  $config->apa->aTag
                );

$db = new DB(
      $servername, 
      $username, 
      $password, 
      $dbname, 
      $port
      );

$searchPage = new SearchPage();
$searched_items = $db->getSearchItems();
$retry = 0;
$result = array();
printf("Date: %s . there are %d items to search \r\n", date("Y-m-d h:i:s"), count($searched_items, 1));
foreach ($searched_items as $keyword => $asins) {
  for ($i = 1; $i <= 10 ; ){  //search until the page 10
    try{
      $flag = 0; 
      $ranks = $searchPage->request($keyword, $i);
      printf($searchPage->getUrl()."\r\n");
      $returned_asins = array_keys($ranks);
      foreach ($asins as $asin) {
        if(in_array($asin, $returned_asins)){
          $result[$asin] = $ranks[$asin];
          printf("find item %s, rank: %s \r\n", $asin, $ranks[$asin]);
          $db->saveSearchRankResult($asin, $keyword, $ranks[$asin]);
          $flag = 1;
          break; //break the search loop if the asin founded 
        }
      }
      $i++;
      $retry = 0;

      if($flag){
        printf("Founded, jump to next keywords. \r\n");
        break;
      }
    }catch(Exception $e){
      echo "retry: ".$retry." ".$e->getMessage();
      print_r(debug_backtrace());
      if($retry == MAX_RETRY){
        $retry = 0;
        $i++;
      }
      $retry++;
      sleep(5);
    }
  }
}
print_r($result);



